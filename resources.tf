resource "aws_acm_certificate" "domain" {
  provider                  = aws.acm
  domain_name               = var.domain
  subject_alternative_names = [for domain in var.additional_domains : domain]
  validation_method         = "DNS"

  tags = merge(var.tags, {
    Name = var.domain
  })
}

resource "aws_route53_record" "domain_validation" {
  for_each = {
    for option in aws_acm_certificate.domain.domain_validation_options : option.domain_name => {
      name   = option.resource_record_name
      record = option.resource_record_value
      type   = option.resource_record_type
    }
  }
  allow_overwrite = true
  name            = each.value.name
  records         = [each.value.record]
  ttl             = 60
  type            = each.value.type
  zone_id         = var.dns_zone
}

resource "aws_acm_certificate_validation" "domain" {
  provider                = aws.acm
  certificate_arn         = aws_acm_certificate.domain.arn
  validation_record_fqdns = [for record in aws_route53_record.domain_validation : record.fqdn]
}
