# ACM Certificate | AWS | Terraform Modules | Twuni

This Terraform module provisions a valid X.509 certificate for a given
domain (+ additional domains, if any). It differs from a plain
`aws_acm_certificate` resource in that it also completes domain
validation for the created certificate by creating the relevant
domain validation records in the appropriate Route 53 hosted zone.

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | ~> 1.0.3 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 3.52.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_additional_domains"></a> [additional\_domains](#input\_additional\_domains) | Any additional domains for which the certificate will be valid. | `list(string)` | `[]` | no |
| <a name="input_dns_zone"></a> [dns\_zone](#input\_dns\_zone) | The ID of the Route 53 hosted zone to which the given domain belongs. | `string` | n/a | yes |
| <a name="input_domain"></a> [domain](#input\_domain) | The hostname for which the certificate will be valid. | `string` | n/a | yes |
| <a name="input_tags"></a> [tags](#input\_tags) | Tags to be set on all resources provisioned by this module. | `map(string)` | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_certificate_arn"></a> [certificate\_arn](#output\_certificate\_arn) | The ARN of an X.509 certificate valid for the given domain (+ the additional domains, if given). |
