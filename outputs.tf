output "certificate_arn" {
  description = "The ARN of an X.509 certificate valid for the given domain (+ the additional domains, if given)."
  value       = aws_acm_certificate.domain.arn
}
