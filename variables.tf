variable "additional_domains" {
  default     = []
  description = "Any additional domains for which the certificate will be valid."
  type        = list(string)
}

variable "dns_zone" {
  description = "The ID of the Route 53 hosted zone to which the given domain belongs."
  type        = string
}

variable "domain" {
  description = "The hostname for which the certificate will be valid."
  type        = string
}

variable "tags" {
  default     = {}
  description = "Tags to be set on all resources provisioned by this module."
  type        = map(string)
}
